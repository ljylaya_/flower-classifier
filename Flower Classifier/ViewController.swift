//
//  ViewController.swift
//  Flower Classifier
//
//  Created by Leah Joy Ylaya on 12/22/20.
//

import UIKit
import CoreML
import Vision
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var flowerName: UILabel!
    @IBOutlet weak var flowerDescription: UILabel!
    let imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = false
    }

    @IBAction func openCameraTapped(_ sender: UIButton) {
        present(imagePicker, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = image
            
            guard let ciimage = CIImage(image: image) else {
                fatalError("Error converting image to CIImage")
            }
            
            detect(image: ciimage)
        }
        imagePicker.dismiss(animated: true)
    }
    
    func detect(image: CIImage) {
        let config = MLModelConfiguration()
        guard let model = try? VNCoreMLModel(for: FlowerClassifier(configuration: config).model) else {
            fatalError("Error loading Model")
        }
        
        let request = VNCoreMLRequest(model: model){ [weak self] request, error in
            guard let results = request.results as? [VNClassificationObservation] else {
                fatalError("Model failed to process image")
            }
            if let firstResult = results.first {
                let folowername = firstResult.identifier.capitalized
                self?.flowerName.text = folowername
                self?.fetchFlower(flowerName: folowername)
            }
        }
        
        let handler = VNImageRequestHandler(ciImage: image)
        do {
            try handler.perform([request])
        } catch {
            fatalError("VNImageRequestHandler \(error)")
        }
    }
    
    func fetchFlower(flowerName: String) {
        let parameters : [String:String] = ["format" : "json", "action" : "query", "prop" : "extracts|pageimages", "exintro" : "", "explaintext" : "", "titles" : flowerName, "redirects" : "1", "pithumbsize" : "500", "indexpageids" : ""]
        let link = "https://en.wikipedia.org/w/api.php"
        
        let request = AF.request(link, method: .get, parameters: parameters)
        request.responseJSON { [weak self] response in
            guard let self = `self` else { return }
            switch response.result {
            case .success(let data):
                let flowerJSON : JSON = JSON(data)
                let pageid = flowerJSON["query"]["pageids"][0].stringValue
                let description = flowerJSON["query"]["pages"][pageid]["extract"].stringValue
                self.flowerDescription.text = description
                break
            case .failure(let error):
                self.showAlert(with: error.localizedDescription)
                break
            }
            debugPrint(response.result)
        }
    }
    
    func showAlert(with message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true)
    }
}

